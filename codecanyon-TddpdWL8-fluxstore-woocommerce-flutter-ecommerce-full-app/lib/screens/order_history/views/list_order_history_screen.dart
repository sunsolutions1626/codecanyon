import 'package:flutter/material.dart';
import 'package:fstore/routes/flux_navigate.dart';
import 'package:provider/provider.dart';

import '../../../common/constants.dart';
import '../../../generated/l10n.dart';
import '../../../models/index.dart';
import '../../../services/index.dart';
import '../../../widgets/common/paging_list.dart';
import '../../common/app_bar_mixin.dart';
import '../models/list_order_history_model.dart';
import '../models/order_history_detail_model.dart';
import 'widgets/order_list_item.dart';
import 'widgets/order_list_loading_item.dart';

class ListOrderHistoryScreen extends StatefulWidget {
  @override
  State<ListOrderHistoryScreen> createState() => _ListOrderHistoryScreenState();
}

class _ListOrderHistoryScreenState extends State<ListOrderHistoryScreen>
    with AppBarMixin {
  ListOrderHistoryModel get listOrderViewModel =>
      Provider.of<ListOrderHistoryModel>(context, listen: false);
  int c = 0;
  var mapOrderHistoryDetailModel = <int, OrderHistoryDetailModel>{};
  bool sa = false;
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserModel>(context, listen: false).user ?? User();
    return renderScaffold(
      routeName: RouteList.orders,
      body: Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          title: Text(
            S.of(context).orderHistory,
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
          centerTitle: true,
          backgroundColor: Theme.of(context).backgroundColor,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_sharp,
              color: Theme.of(context).colorScheme.secondary,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        body: PagingList<ListOrderHistoryModel, Order>(
          onRefresh: mapOrderHistoryDetailModel.clear,
          header: c == 1
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Checkbox(
                        shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(4))),
                        value: sa,
                        // focusColor:
                        //     AppColors.grey9b,
                        // activeColor:
                        //     AppColors.greyf22,
                        // checkColor:
                        //     AppColors.white,
                        onChanged: (value) {
                          setState(() {
                            sa = !sa;
                            for (int i = 0;
                                i < listOrderViewModel.data!.length;
                                i++) {
                              setState(() {
                                listOrderViewModel.data![i].selected = sa;
                              });
                            }
                          });
                        }),
                    Padding(
                      padding: const EdgeInsets.only(right: 18),
                      child: Row(
                        children: [
                          GestureDetector(
                              child: Icon(Icons.cancel),
                              onTap: () {
                                for (int i = 0;
                                    i < listOrderViewModel.data!.length;
                                    i++) {
                                  setState(() {
                                    c = 0;
                                    listOrderViewModel.data![i].tap = false;
                                    listOrderViewModel.data![i].selected =
                                        false;
                                    sa = false;
                                  });
                                }
                              }),
                          SizedBox(width: 10),
                          GestureDetector(
                              child: Icon(Icons.delete),
                              onTap: () async {
                                int count = 0;
                                for (int i = 0;
                                    i < listOrderViewModel.data!.length;
                                    i++) {
                                  if (listOrderViewModel.data![i].selected ==
                                      true) {
                                    setState(() {
                                      count = 1;
                                    });
                                    Services sc = Services();

                                    await sc.api.deleteOrder(
                                        listOrderViewModel.data![i].id,
                                        user.cookie!);
                                  }
                                }
                                if (count == 1) {
                                  final user2 = Provider.of<UserModel>(context,
                                          listen: false)
                                      .user;
                                  Navigator.pop(context);
                                  await FluxNavigate.pushNamed(
                                    RouteList.orders,
                                    arguments: user2,
                                  );
                                } else {
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(SnackBar(
                                    backgroundColor: Color(0xffFF4500),
                                    content: Text("Nothing to Delete!!",
                                        style: TextStyle(color: Colors.white)),
                                    duration: const Duration(seconds: 2),
                                  ));
                                }
                              }),
                        ],
                      ),
                    )
                  ],
                )
              : Container(),
          itemBuilder: (_, order, index) {
            if (mapOrderHistoryDetailModel[index] == null) {
              final orderHistoryDetailModel = OrderHistoryDetailModel(
                order: order,
                user: user,
              );
              mapOrderHistoryDetailModel[index] = orderHistoryDetailModel;
            }
            return ChangeNotifierProvider<OrderHistoryDetailModel>.value(
              value: mapOrderHistoryDetailModel[index]!,
              child: GestureDetector(
                onLongPress: () {
                  setState(() {
                    c = 1;
                    for (int i = 0; i < listOrderViewModel.data!.length; i++) {
                      setState(() {
                        listOrderViewModel.data![i].tap = true;
                      });
                    }
                  });
                },
                child: Row(
                  children: [
                    mapOrderHistoryDetailModel[index]!.order.tap == true
                        ? Checkbox(
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(4))),
                            value: order.selected,
                            // focusColor:
                            //     AppColors.grey9b,
                            // activeColor:
                            //     AppColors.greyf22,
                            // checkColor:
                            //     AppColors.white,
                            onChanged: (value) {
                              setState(() {
                                order.selected = !order.selected;
                              });
                              int count = 0;
                              for (int i = 0;
                                  i < listOrderViewModel.data!.length;
                                  i++) {
                                if (listOrderViewModel.data![i].selected ==
                                    true) {
                                  count++;
                                }
                              }
                              if (count != listOrderViewModel.data!.length) {
                                setState(() {
                                  sa = false;
                                });
                              } else {
                                setState(() {
                                  sa = true;
                                });
                              }
                            })
                        : Container(),
                    OrderListItem(),
                  ],
                ),
              ),
            );
          },
          lengthLoadingWidget: 3,
          loadingWidget: const OrderListLoadingItem(),
        ),
      ),
    );
  }
}
