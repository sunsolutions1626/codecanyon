// ignore_for_file: prefer_single_quotes, lines_longer_than_80_chars final
Map<String, dynamic> environment = {
  "appConfig": "lib/config/config_en.json",
  "serverConfig": {
    "url": "https://zapagaming.co.za",
    "type": "woo",
    "consumerKey": "ck_706d5c5432ddaf46edd757d4775e7b25f611bd54",
    "consumerSecret": "cs_dcd83d85b18dae01e5f06e641070fe405d2ed5c4"
  },
  "defaultDarkTheme": true,
  "enableRemoteConfigFirebase": false,
  "loginSMSConstants": {
    "countryCodeDefault": "VN",
    "dialCodeDefault": "+84",
    "nameDefault": "Vietnam"
  },
  "storeIdentifier": {"disable": true, "android": "", "ios": ""},
  "advanceConfig": {
    "DefaultLanguage": "en",
    "DetailedBlogLayout": "halfSizeImageType",
    "EnablePointReward": true,
    "hideOutOfStock": false,
    "HideEmptyTags": true,
    "HideEmptyCategories": true,
    "EnableRating": true,
    "hideEmptyProductListRating": true,
    "EnableCart": true,
    "ShowBottomCornerCart": true,
    "EnableSkuSearch": true,
    "showStockStatus": true,
    "GridCount": 3,
    "isCaching": false,
    "kIsResizeImage": false,
    "httpCache": true,
    "Currencies": [
      {
        "symbol": "R",
        "decimalDigits": 2,
        "symbolBeforeTheNumber": true,
        "currency": "Rand",
        "currencyCode": "ZAR",
        "smallestUnitRate": 1
      }
    ],
    "DefaultStoreViewCode": "",
    "EnableAttributesConfigurableProduct": ["color", "size"],
    "EnableAttributesLabelConfigurableProduct": ["color", "size"],
    "isMultiLanguages": true,
    "EnableApprovedReview": false,
    "EnableSyncCartFromWebsite": false,
    "EnableSyncCartToWebsite": false,
    "EnableFirebase": true,
    "RatioProductImage": 1.2,
    "EnableCouponCode": false,
    "ShowCouponList": true,
    "ShowAllCoupons": true,
    "ShowExpiredCoupons": true,
    "AlwaysShowTabBar": false,
    "PrivacyPoliciesPageId": 25569,
    "PrivacyPoliciesPageUrl": "https://mstore.io/",
    "SupportPageUrl": "https://support.inspireui.com/",
    "DownloadPageUrl": "https://mstore.io/#download",
    "SocialConnectUrl": [
      {
        "name": "Facebook",
        "icon": "assets/icons/logins/facebook.png",
        "url": "https://www.facebook.com/inspireui"
      },
      {
        "name": "Instagram",
        "icon": "assets/icons/logins/instagram.png",
        "url": "https://www.instagram.com/inspireui9/"
      }
    ],
    "AutoDetectLanguage": false,
    "QueryRadiusDistance": 10,
    "MinQueryRadiusDistance": 1,
    "MaxQueryRadiusDistance": 10,
    "EnableMembershipUltimate": false,
    "EnablePaidMembershipPro": false,
    "EnableDeliveryDateOnCheckout": true,
    "EnableNewSMSLogin": false,
    "EnableBottomAddToCart": false,
    "inAppWebView": false,
    "EnableWOOCSCurrencySwitcher": false,
    "enableProductBackdrop": false,
    "categoryImageMenu": false,
    "EnableDigitsMobileLogin": false,
    "OnBoardOnlyShowFirstTime": true,
    "WebViewScript": "",
    "EnableVersionCheck": false,
    "AjaxSearchURL": "",
    "AlwaysClearWebViewCache": false,
    "OrderNotesWithPrivateNote": true,
    "EnableShipping": true,
    "DefaultCurrency": {
      "symbol": "R",
      "decimalDigits": 2,
      "symbolBeforeTheNumber": true,
      "currency": "Rand",
      "currencyCode": "ZAR",
      "smallestUnitRate": 1
    },
    "EnableShoppingCart": true
  },
  "defaultDrawer": {
    "logo": "assets/images/logo.png",
    "items": [
      {"type": "home", "show": true},
      {"type": "blog", "show": true},
      {"type": "categories", "show": true},
      {"type": "cart", "show": true},
      {"type": "profile", "show": true},
      {"type": "login", "show": true},
      {"type": "category", "show": true}
    ]
  },
  "defaultSettings": [
    "products",
    "chat",
    "wishlist",
    "notifications",
    "language",
    "currencies",
    "darkTheme",
    "order",
    "point",
    "rating",
    "privacy",
    "about"
  ],
  "loginSetting": {
    "IsRequiredLogin": false,
    "showAppleLogin": false,
    "showFacebook": false,
    "showSMSLogin": false,
    "showGoogleLogin": false,
    "showPhoneNumberWhenRegister": false,
    "requirePhoneNumberWhenRegister": false
  },
  "oneSignalKey": {
    "enable": true,
    "appID": "1384a48a-ac8f-44db-a851-5b5e6b24c083"
  },
  "onBoardingData": [
    {
      "title": "Welcome ",
      "desc": "The Revolution Is Here.",
      "image":
          "https://zapagaming.co.za/wp-content/uploads/2022/05/splash-screen-square.jpg"
    },
    {
      "title": "Let's Get Started",
      "desc": "",
      "image":
          "https://zapagaming.co.za/wp-content/uploads/2022/05/splash-screen-square.jpg"
    }
  ],
  "vendorOnBoardingData": [
    {
      "title": "Welcome aboard",
      "image": "assets/images/searching.png",
      "desc": "Just a few more steps to become our vendor"
    },
    {
      "title": "Let's Get Started",
      "image": "assets/images/manage.png",
      "desc": "Good Luck for great beginnings."
    }
  ],
  "adConfig": {
    "enable": false,
    "facebookTestingId": "",
    "googleTestingId": ["457"],
    "ads": [
      {
        "type": "banner",
        "provider": "google",
        "androidId": "ca-app-pub-3940256099942544/6300978111",
        "iosId": "ca-app-pub-3940256099942544/2934735716",
        "showOnScreens": ["home", "search"],
        "hideOnScreens": []
      },
      {
        "type": "banner",
        "provider": "google",
        "androidId": "ca-app-pub-2101182411274198/4052745095",
        "iosId": "ca-app-pub-2101182411274198/5418791562",
        "hideOnScreens": []
      },
      {
        "type": "interstitial",
        "provider": "google",
        "androidId": "ca-app-pub-3940256099942544/4411468910",
        "iosId": "ca-app-pub-3940256099942544/4411468910",
        "hideOnScreens": []
      },
      {
        "type": "reward",
        "provider": "google",
        "androidId": "ca-app-pub-3940256099942544/4411468910",
        "iosId": "ca-app-pub-3940256099942544/1712485313",
        "hideOnScreens": []
      },
      {
        "type": "banner",
        "provider": "facebook",
        "androidId": "IMG_16_9_APP_INSTALL#430258564493822_489007588618919",
        "iosId": "IMG_16_9_APP_INSTALL#430258564493822_876131259906548",
        "hideOnScreens": []
      },
      {
        "type": "interstitial",
        "provider": "facebook",
        "androidId": "IMG_16_9_APP_INSTALL#430258564493822_489092398610438",
        "iosId": "430258564493822_489092398610438",
        "hideOnScreens": []
      }
    ]
  },
  "firebaseDynamicLinkConfig": {
    "isEnabled": true,
    "uriPrefix": "https://zapagaming.page.link",
    "link": "https://zapagaming.co.a",
    "androidPackageName": "za.co.zapagaming",
    "androidAppMinimumVersion": 1,
    "iOSBundleId": "za.co.zapagaming.co.za",
    "iOSAppMinimumVersion": "1.0.1",
    "iOSAppStoreId": ""
  },
  "languagesInfo": [
    {
      "name": "English",
      "icon": "assets/images/country/gb.png",
      "code": "en",
      "text": "English",
      "storeViewCode": ""
    }
  ],
  "unsupportedLanguages": ["ku"],
  "paymentConfig": {
    "DefaultCountryISOCode": "ZAR",
    "DefaultStateISOCode": "JHB",
    "EnableShipping": true,
    "EnableAddress": true,
    "EnableCustomerNote": true,
    "EnableAddressLocationNote": false,
    "EnableAlphanumericZipCode": false,
    "EnableReview": true,
    "allowSearchingAddress": true,
    "GuestCheckout": false,
    "EnableOnePageCheckout": true,
    "NativeOnePageCheckout": false,
    "CheckoutPageSlug": {"en": "checkout"},
    "EnableCreditCard": false,
    "UpdateOrderStatus": true,
    "ShowOrderNotes": true,
    "EnableRefundCancel": false
  },
  "payments": {
    "paypal": "assets/icons/payment/paypal.png",
    "stripe": "assets/icons/payment/stripe.png",
    "razorpay": "assets/icons/payment/razorpay.png",
    "tap": "assets/icons/payment/tap.png"
  },
  "stripeConfig": {
    "serverEndpoint": "https://stripe-server.vercel.app",
    "publishableKey": "pk_test_MOl5vYzj1GiFnRsqpAIHxZJl",
    "enabled": false,
    "paymentMethodId": "stripe",
    "returnUrl": "fluxstore://inspireui.com",
    "enableManualCapture": false
  },
  "paypalConfig": {
    "clientId":
        "ASlpjFreiGp3gggRKo6YzXMyGM6-NwndBAQ707k6z3-WkSSMTPDfEFmNmky6dBX00lik8wKdToWiJj5w",
    "secret":
        "ECbFREri7NFj64FI_9WzS6A0Az2DqNLrVokBo0ZBu4enHZKMKOvX45v9Y1NBPKFr6QJv2KaSp5vk5A1G",
    "production": false,
    "paymentMethodId": "paypal",
    "enabled": false
  },
  "razorpayConfig": {
    "keyId": "rzp_test_SDo2WKBNQXDk5Y",
    "keySecret": "RrgfT3oxbJdaeHSzvuzaJRZf",
    "paymentMethodId": "razorpay",
    "enabled": false
  },
  "tapConfig": {
    "SecretKey": "sk_test_XKokBfNWv6FIYuTMg5sLPjhJ",
    "paymentMethodId": "tap",
    "enabled": true
  },
  "mercadoPagoConfig": {
    "accessToken":
        "TEST-5726912977510261-102413-65873095dc5b0a877969b7f6ffcceee4-613803978",
    "production": false,
    "paymentMethodId": "woo-mercado-pago-basic",
    "enabled": false
  },
  "payTmConfig": {
    "paymentMethodId": "paytm",
    "merchantId": "your-merchant-id",
    "production": false,
    "enabled": true
  },
  "defaultCountryShipping": [
    {"name": "South Africa", "iosCode": "ZA", "icon": "World"}
  ],
  "afterShip": {"api": "", "tracking_url": ""},
  "productDetail": {
    "height": 0.4,
    "marginTop": 0.0,
    "safeArea": false,
    "showVideo": false,
    "showThumbnailAtLeast": 1,
    "layout": "simpleType",
    "borderRadius": 3.0,
    "ShowSelectedImageVariant": true,
    "ForceWhiteBackground": false,
    "AutoSelectFirstAttribute": true,
    "enableReview": false,
    "attributeImagesSize": 50.0,
    "showSku": false,
    "showStockQuantity": true,
    "showProductCategories": true,
    "showProductTags": false,
    "hideInvalidAttributes": false,
    "autoPlayGallery": false,
    "allowMultiple": false,
    "showBrand": false,
    "showQuantityInList": false,
    "showAddToCartInSearchResult": false,
    "productListItemHeight": 125,
    "boxFit": "cover",
    "SliderShowGoBackButton": true,
    "SliderIndicatorType": "number"
  },
  "blogDetail": {
    "showComment": true,
    "showHeart": true,
    "showSharing": true,
    "showTextAdjustment": true,
    "enableAudioSupport": false
  },
  "productVariantLayout": {
    "color": "color",
    "size": "box",
    "height": "option",
    "color-image": "image"
  },
  "productAddons": {
    "allowImageType": true,
    "allowVideoType": true,
    "allowCustomType": true,
    "allowedCustomType": ["png", "pdf", "docx"],
    "allowMultiple": false,
    "fileUploadSizeLimit": 5.0
  },
  "cartDetail": {"minAllowTotalCartValue": 0, "maxAllowQuantity": 10},
  "productVariantLanguage": {
    "en": {
      "color": "Color",
      "size": "Size",
      "height": "Height",
      "color-image": "Color"
    },
    "ar": {
      "color": "اللون",
      "size": "بحجم",
      "height": "ارتفاع",
      "color-image": "اللون"
    },
    "vi": {
      "color": "Màu",
      "size": "Kích thước",
      "height": "Chiều Cao",
      "color-image": "Màu"
    }
  },
  "excludedCategory": 311,
  "saleOffProduct": {"ShowCountDown": true, "Color": "#C7222B"},
  "notStrictVisibleVariant": true,
  "configChat": {
    "EnableSmartChat": true,
    "showOnScreens": [
      "profile",
      "home",
      "orders",
      "product",
      "products",
      "dynamic"
    ],
    "hideOnScreens": []
  },
  "smartChat": [
    {"app": "https://wa.me/27823010366", "iconData": "whatsapp"},
    {"app": "tel:+27823010366", "iconData": "phone"}
  ],
  "adminEmail": "dj@zapagaming.co.za",
  "adminName": "Zapa Gaming",
  "vendorConfig": {
    "VendorRegister": true,
    "DisableVendorShipping": false,
    "ShowAllVendorMarkers": true,
    "DisableNativeStoreManagement": false,
    "dokan": "my-account?vendor_admin=true",
    "wcfm": "store-manager?vendor_admin=true",
    "DisableMultiVendorCheckout": false,
    "DisablePendingProduct": false,
    "EnableAutoApplicationApproval": false
  },
  "deliveryConfig": {"DisableDeliveryManagement": false},
  "loadingIcon": {"type": "threeBounce", "size": 30.0},
  "splashScreen": {
    "type": "static",
    "image":
        "https://zapagaming.co.za/wp-content/uploads/2022/05/splash-screen-new-white-2.jpg",
    "animationName": "",
    "enable": true,
    "duration": 2000
  },
  "productCard": {
    "hidePrice": false,
    "hideStore": false,
    "hideTitle": false,
    "borderRadius": 3.0,
    "boxFit": "fill",
    "boxShadow": {"x": 0.0, "y": 0.0, "blurRadius": 0.0}
  },
  "darkConfig": {
    "MainColor": "ffff5722",
    "logo": "https://i.imgur.com/jSxDpuC.png"
  },
  "lightConfig": {
    "MainColor": "ffff5722",
    "logo": "https://i.imgur.com/msTbioA.png"
  }
};
